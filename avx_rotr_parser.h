#pragma once

#include <x86intrin.h>
#include <iostream>
#include <algorithm>
#include "util.h"
#include "const.h"

// The result of this function is the mask that would mute bits that are part of a sting
inline __mmask64 compute_quote_mute_mask(__mmask64& quote_mute_mask, __mmask64 quote_mask, char* current_buf_data) {
    __mmask64 result = quote_mute_mask;
    __mmask64 num_quote = __builtin_popcountl(quote_mask);
    if (num_quote == 0) {
        return result;
    }
    __mmask64 rot = 0;
    for (__mmask64 i = 0; i < num_quote - 1; ++i) {
        __mmask64 quote_pos = __builtin_ctzl(quote_mask);
        if (is_escaped(current_buf_data - 2 * vector_size + rot + quote_pos)) {
            quote_mask &= ~(1ul << quote_pos); // if " is escaped just mute it
            continue;
        }
        quote_mute_mask = ~quote_mute_mask;
        result ^= ~((1ul << quote_pos) - 1) & (~__mmask64(0) >> rot);
        result = __rorq(result, quote_pos + 1);
        quote_mask = __rorq(quote_mask, quote_pos+1);
        rot += quote_pos + 1;
    }
    __mmask64 quote_pos = __builtin_ctzl(quote_mask) + 1;
    if (!is_escaped(current_buf_data - 2 * vector_size + rot + quote_pos)) {
        result ^= ~((1ul << quote_pos)  - 1) & (~__mmask64(0) >> rot);
        quote_mute_mask = ~quote_mute_mask;
    }
    result = __rolq(result, rot);
    return result;
}

// Broken and somewhat slow , but idea is there
template<>
class Parser<ParseMode::AVXRot> {
public:
    void ProcessReader(Reader<read_mode>& reader) {
        const __m256i quote_vec = _mm256_set1_epi8('"');
        const __m256i colon_vec = _mm256_set1_epi8(':');
        const __m256i open_bracket_vec = _mm256_set1_epi8('{');
        const __m256i close_bracket_vec = _mm256_set1_epi8('}');

        size_t current_line_num_matches = 0;
        __mmask64 quote_mute_mask = ~__mmask64(0);
        size_t object_level = 0;

        size_t buf_size = 0;
        bool done = false;
        while (!done) {
            auto buf = reader.GetBuffer(buf_size);
            if (buf_size == -1) {
                throw std::runtime_error("read error");
            }
            auto current_buf_data = buf->data;

            done = adjust_buf(current_buf_data, buf_size);
            char* end = current_buf_data + buf_size;

            struct {
                union {
                    __mmask64 as64;
                    __mmask32 as32[2];
                };
            } quote_mask, colon_mask, open_bracket_mask, close_bracket_mask;
            int num = 0;
            while (current_buf_data < end) {
                quote_mask.as32[0] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)), quote_vec);
                colon_mask.as32[0] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)), colon_vec);
                open_bracket_mask.as32[0] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)),
                                                                   open_bracket_vec);
                close_bracket_mask.as32[0] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)),
                                                                    close_bracket_vec);
                current_buf_data += vector_size;

                quote_mask.as32[1] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)), quote_vec);
                colon_mask.as32[1] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)), colon_vec);
                open_bracket_mask.as32[1] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)),
                                                                   open_bracket_vec);
                close_bracket_mask.as32[1] = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)),
                                                                    close_bracket_vec);
                current_buf_data += vector_size;

//                std::q << std::string(current_buf_data - 64, 64) << std::endl;
//                std::cerr << mask_to_string(quote_mask.as64) << std::endl;
//                std::cerr << mask_to_string(colon_mask.as64) << std::endl;
//                std::cerr << mask_to_string(open_bracket_mask.as64) << std::endl;
//                std::cerr << mask_to_string(close_bracket_mask.as64) << std::endl;
                __mmask64 cur_quote_mute_mask = compute_quote_mute_mask(quote_mute_mask, quote_mask.as64,
                                                                        current_buf_data);
//                std::cerr << std::string(current_buf_data - 64, 64) << std::endl;
//                std::cerr << "quote mute masks:" << std::endl;
//                std::cerr << mask_to_string(cur_quote_mute_mask) << std::endl;
//                std::cerr << mask_to_string(quote_mute_mask) << std::endl;

                open_bracket_mask.as64 &= cur_quote_mute_mask;
                close_bracket_mask.as64 &= cur_quote_mute_mask;
                colon_mask.as64 &= cur_quote_mute_mask;
//                std::cerr << mask_to_string(colon_mask.as64) << std::endl;
//                std::cerr << mask_to_string(open_bracket_mask.as64) << std::endl;
//                std::cerr << mask_to_string(colon_mask.as64) << std::endl;

                __mmask64 object_mute_mask = object_level > 2 ? __mmask64(0) : ~__mmask64(0);
                __mmask64 bracket_mask = open_bracket_mask.as64 | close_bracket_mask.as64;
//                std::cerr << mask_to_string(bracket_mask) << std::endl;
                while (bracket_mask) {
                    __mmask64 bracket_pos = __builtin_ctzl(bracket_mask);
                    bracket_mask &= ~(1ul << bracket_pos);
                    if (open_bracket_mask.as64 & (1ul << bracket_pos)) {
                        // open bracket
                        ++object_level;
                        if (object_level == 3) {
                            object_mute_mask &= ((1ul << bracket_pos) - 1);
                        }
                    } else {
                        // close bracket
                        --object_level;
                        if (object_level == 2) {
                            object_mute_mask |= ~((1ul << bracket_pos) - 1);
                        } else if (object_level == 0) {
                            ++num_lines;
                            //std::cerr << current_line_num_matches << std::endl;
                            current_line_num_matches += __builtin_popcountl(
                                colon_mask.as64 & object_mute_mask & ((1ul << bracket_pos) - 1)
                            );
                            //std::cerr << mask_to_string(colon_mask.as64 & object_mute_mask & ((1ul << bracket_pos) - 1)) << std::endl;
                            //std::cerr << current_line_num_matches << " " << bracket_pos << std::endl;

                            if ((current_line_num_matches & 1) == 0) {
                                ++result;
                            }
                            current_line_num_matches = 0;
                            colon_mask.as64 &= ~((1ul << bracket_pos) - 1);
                        }
                    }
                }
                current_line_num_matches += __builtin_popcountl(colon_mask.as64 & object_mute_mask);

//                std::string s(current_buf_data - 64, 64);
//                std::replace(s.begin(), s.end(), '\n', ' ');
//                std::cerr << s << std::endl;
//                std::cerr << mask_to_string(cur_quote_mute_mask) << std::endl;
//                std::cerr << mask_to_string(object_mute_mask) << std::endl;
//                std::cerr << mask_to_string(colon_mask.as64 & object_mute_mask) << " " <<  __builtin_popcountl(colon_mask.as64 & object_mute_mask) << " " << current_line_num_matches << std::endl;
            }
            reader.DoneWithBuffer(buf);
        }

    }

    size_t Result() { return result; }
    size_t NumLines() { return num_lines; }
    size_t NumFallback() { return 0; }

private:
    size_t result = 0;
    size_t num_lines = 0;
};