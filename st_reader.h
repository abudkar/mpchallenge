#pragma once

#include <cstring>
#include <stdexcept>
#include <unistd.h>
#include "const.h"
#include "alloc_data.h"

class StBuffer {
public:
    StBuffer(): allocated((char*)alloc_data()), data(allocated+page_size) {
    }

    ~StBuffer() {
        free_data(allocated);
    }

private:
    char* const allocated;
public:
    char* const data;
};

template<>
class Reader<ReadMode::SingleThread> {
public:
    typedef StBuffer BufferType;

    Reader(int fd) : fd(fd) {
        size_t rd = read(fd, buf.data+single_buffer_capacity, page_size);
        if (rd < page_size || rd == -1) {
            throw std::runtime_error("bad_input");
        }
    }

    BufferType* GetBuffer(size_t& rd) {
        memcpy(buf.data-page_size, buf.data+single_buffer_capacity-page_size, 2*page_size);
        rd = read(fd, buf.data+page_size, single_buffer_capacity);
        return &buf;
    }

    void DoneWithBuffer(BufferType*) {}
private:
    StBuffer buf;
    int fd;
};