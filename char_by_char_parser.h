#pragma once

#include "const.h"
#include "util.h"

template<>
class Parser<ParseMode::CharByChar> {
public:
    void ProcessReader(Reader<read_mode>& reader) {
        process_by_line(reader, *this);
    }

    void ProcessLine(char* buf_data) {
        ++num_lines;
        size_t obj_level = 0;
        bool escaped = false;
        bool done = false;
        size_t num_prop = 0;
        while (!done) {
            switch (*buf_data) {
                case '"':
                    do {
                        buf_data = strchr(buf_data+1, '"');
                    } while (is_escaped(buf_data));
                    break;
                case '{':
                    ++obj_level;
                    break;
                case '}':
                    done = --obj_level==0;
                    break;
                case ':':
                    if (obj_level == 2) {
                        ++num_prop;
                    }
            }
            ++buf_data;
        }
        if ((num_prop & 1) == 0) {
            ++result;
        }
    }

    size_t Result() { return result; }
    size_t NumLines() { return num_lines; }
    size_t NumFallback() { return num_lines; }

private:
    size_t num_lines = 0;
    size_t result = 0;
};