#!/bin/bash
if [ ! -e "./output.json" ]; then
  echo "Downloading test output for profile generation"
  gsutil cp -p gs://mpchallenge1/output.json ./
fi

#NOTE add -pthread if compiling with `constexpr ReadMode read_mode = ReadMode::MultiThreaded;`

echo "Building instrumented binary"
g++-9 -std=c++17  -march=skylake-avx512 -mtune=skylake-avx512 -O3 -fprofile-generate main.cpp -o ./mpchallenge-instrumented


echo "Running instrumented binary to collect profile"
if [ -e "./main.gcda" ]; then
  rm ./main.gcda
fi
./mpchallenge-instrumented ./output.json > /dev/null
rm ./mpchallenge-instrumented


echo "Building final binary"
g++-9 -std=c++17  -march=skylake-avx512 -mtune=skylake-avx512 -O3 -fprofile-use main.cpp -o ./mpchallenge


echo 'Echo all done, use `./mpchallenge ${PATH_TO_INPUT}`'