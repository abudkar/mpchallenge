#pragma once

#include "const.h"
#include "sax_parser.h"
#include "char_by_char_parser.h"

template<>
class Parser<ParseMode::AVXShiftWithFallback> {
public:
    void ProcessReader(Reader<read_mode>& reader) {
        const __m256i nl_vec = _mm256_set1_epi8('\n');
        const __m256i slash_vec = _mm256_set1_epi8('\\');
        const __m256i colon_vec = _mm256_set1_epi8(':');
        const __m256i quote_vec = _mm256_set1_epi8('"');
        const __m256i open_bracket_vec = _mm256_set1_epi8('{');

        char cary_over[128 * 1024];
        size_t cary_over_len = 0;

        size_t current_line_num_matches = 0;
        bool current_line_disqualified = false;
        uint32_t current_line_open_bracket = 0;

        size_t buf_size = 0;
        size_t file_pos = 0;

        bool done = false;
        while (!done) {
            auto buf = reader.GetBuffer(buf_size);
            if (buf_size == -1) {
                throw std::runtime_error("read error");
            }
            file_pos += buf_size;
            auto current_buf_data = buf->data;
            auto current_line_start = current_buf_data;

            done = adjust_buf(current_buf_data, buf_size);
            char* end = current_buf_data + buf_size;

            for (; current_buf_data < end; current_buf_data+=vector_size) {
                __mmask32 nl_mask = _mm256_cmpeq_epi8_mask(*((const __m256i*)(current_buf_data)), nl_vec);
                __m256i shifted_1 = _mm256_lddqu_si256((const __m256i*)(current_buf_data + 1));
                __m256i shifted_2 = _mm256_lddqu_si256((const __m256i*)(current_buf_data + 2));

                __mmask32 open_bracket_mask = _mm256_cmpeq_epi8_mask(*((const __m256i*)(current_buf_data)), open_bracket_vec);
                __mmask32 slash_masks = _mm256_cmpeq_epi8_mask(*((const __m256i*)(current_buf_data)), slash_vec);
                __mmask32 shifted_slash_mask = _mm256_cmpeq_epi8_mask(shifted_1, slash_vec);
                __mmask32 quote_masks_1 = _mm256_cmpeq_epi8_mask(shifted_1, quote_vec);
                __mmask32 colon_masks_0 = _mm256_cmpeq_epi8_mask(*((const __m256i*)(current_buf_data)), colon_vec);
                __mmask32 colon_masks_2 = _mm256_cmpeq_epi8_mask(shifted_2, colon_vec);

                if (nl_mask == 0) {
                    current_line_open_bracket += __builtin_popcount(open_bracket_mask);
                    current_line_disqualified = current_line_disqualified || ((slash_masks & shifted_slash_mask) != 0) || (current_line_open_bracket > 2);
                    current_line_num_matches += __builtin_popcount((~slash_masks) & (~colon_masks_0) & quote_masks_1 & colon_masks_2);
                    continue;
                }

                auto nl_pos = __builtin_ctz(nl_mask);
                __mmask32 current_line_mask = nl_mask - 1;
                __mmask32 next_line_mask = ~current_line_mask;

                current_line_open_bracket += __builtin_popcount(open_bracket_mask & current_line_mask);
                current_line_num_matches += __builtin_popcount((~slash_masks) & (~colon_masks_0) & quote_masks_1 & colon_masks_2 & current_line_mask);
                if (
                    current_line_disqualified ||
                    ((slash_masks & shifted_slash_mask & current_line_mask) != 0) ||
                    (current_line_open_bracket > 2)
                ) {
                    if (cary_over_len == 0) {
                        fallback.ProcessLine(current_line_start);
                    } else if (cary_over_len <= page_size) {
                        fallback.ProcessLine(buf->data - cary_over_len);
                    } else {
                        memcpy(cary_over+cary_over_len, buf->data, (current_buf_data - buf->data) + nl_pos);
                        fallback.ProcessLine(cary_over);
                    }
                } else if ((current_line_num_matches & 1) == 0) {
                    ++result;
                }

                ++num_lines;
                // set next line state
                cary_over_len = 0;
                current_line_start = current_buf_data + nl_pos + 1;
                current_line_open_bracket = __builtin_popcount(open_bracket_mask & next_line_mask);
                current_line_disqualified = (__builtin_popcount(slash_masks & shifted_slash_mask & next_line_mask) != 0) || (current_line_open_bracket > 2);
                current_line_num_matches = __builtin_popcount((~slash_masks) & (~colon_masks_0) & quote_masks_1 & colon_masks_2 & next_line_mask);
            }
            cary_over_len = end - current_line_start;
            if (cary_over_len > 0) {
                if (done) {
                    ++num_lines;
                    if (current_line_disqualified) {
                        fallback.ProcessLine(current_line_start);
                    } else if ((current_line_num_matches & 1) == 0 && (current_line_num_matches > 0)) {
                        ++result;
                    }
                } else if (cary_over_len > page_size) {
                    memcpy(cary_over, current_line_start, cary_over_len);
                }
            }
            reader.DoneWithBuffer(buf);
        }
    }

    size_t Result() { return result + fallback.Result(); }
    size_t NumLines() { return num_lines; }
    size_t NumFallback() { return fallback.NumLines(); }

private:
    size_t result = 0;
    size_t num_lines = 0;

    Parser<ParseMode::CharByChar> fallback;
};