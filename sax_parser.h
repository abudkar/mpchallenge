#pragma once

#include <stdexcept>

#include "rapidjson/reader.h"
#include "util.h"

class Handler : public rapidjson::BaseReaderHandler<rapidjson::UTF8<>, Handler> {
public:
    bool StartObject() {
        ++object_level;
        return true;
    }

    bool EndObject(rapidjson::SizeType numElement) {
        if ((object_level == 2) && ((numElement & 1) == 0)) {
            ++result;
        }
        --object_level;
        return true;
    }

    size_t Result() { return result; }
private:
    int object_level = 0;
    int result = 0;
};

template<>
class Parser<ParseMode::SAX> {
public:
    void ProcessReader(Reader<read_mode>& reader) {
        process_by_line(reader, *this);
    }

    void ProcessLine(char* buf_data) {
        ++num_lines;
        rapidjson::InsituStringStream ss(buf_data);
        rj_reader.Parse<
            rapidjson::kParseInsituFlag |
            rapidjson::kParseStopWhenDoneFlag,
            rapidjson::InsituStringStream,
            Handler
        >(ss, handler);
    }

    size_t Result() { return handler.Result(); }
    size_t NumLines() { return num_lines; }
    size_t NumFallback() { return num_lines; }

private:
    size_t num_lines = 0;
    Handler handler;
    rapidjson::Reader rj_reader;
};