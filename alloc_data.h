#pragma once

#include <cstdlib>
#include <sys/mman.h>
#include <strings.h>
#include "const.h"

inline void* alloc_data() {
    void* result;
    switch (alloc_mode) {
        case AllocMode::Alloc:
            result = aligned_alloc(page_size, single_buffer_capacity + 2 * page_size);
            break;
        case AllocMode::MMap: // Mmap always aligns to page boundary (4k or 2MB normal vs huge)
            result = mmap(NULL, single_buffer_capacity + 2 * page_size, PROT_WRITE | PROT_READ, MAP_ANON | MAP_PRIVATE, -1, 0);
            break;
        case AllocMode::MMapHuge:
            result = mmap(NULL, single_buffer_capacity + huge_page_size, PROT_WRITE | PROT_READ, MAP_ANON | MAP_PRIVATE | MAP_HUGETLB, -1, 0);
            break;
    }
    if (result == MAP_FAILED) {
        if constexpr (alloc_mode == AllocMode::MMapHuge) {
            std::cerr << "Enable huge pages with: `sudo sysctl -w vm.nr_hugepages=100`" << std::endl;
        }
        throw std::bad_alloc();
    }
    bzero(result, single_buffer_capacity+2 * page_size);
    return result;
}

inline void free_data(void* data) {
    switch (alloc_mode) {
        case AllocMode::Alloc:
            free(data);
            break;
        case AllocMode::MMap:
        case AllocMode::MMapHuge:
            munmap(data, single_buffer_capacity);
            break;
    }
}
