#pragma once

#include <immintrin.h>

#include "mt_reader.h"
#include "st_reader.h"
#include "const.h"

inline std::string mask_to_string(__mmask64 mask) {
    std::string result;
    result.reserve(64);
    // intentionally reversed
    for (int i = 0; i < 64; ++i) {
        if ((mask & 1) == 0) {
            result += "0";
        } else {
            result += "1";
        }
        mask >>= 1;
    }
    return result;
}

inline std::string mask_to_string(__mmask32 mask) {
    std::string result;
    result.reserve(32);
    // intentionally reversed
    for (int i = 0; i < 32; ++i) {
        if ((mask & 1) == 0) {
            result += "0";
        } else {
            result += "1";
        }
        mask >>= 1;
    }
    return result;
}

inline bool is_escaped(const char* data) {
    bool result = false;
    while (*(--data) == '\\') {
        result = !result;
    }
    return result;
}

inline bool adjust_buf(char* buf_data, size_t& buf_size) {
    if (buf_size < single_buffer_capacity) {
        bzero(buf_data + page_size + buf_size, 2 * vector_size);
        buf_size = (buf_size & (~size_t(2*vector_size - 1))) + page_size + 2 * vector_size;
        return true;
    }
    return false;
}

template <class LineProcessor>
inline void process_by_line(Reader<read_mode>& reader, LineProcessor& lp) {
    const __m256i nl_vec = _mm256_set1_epi8('\n');
    char cary_over[128 * 1024];
    size_t cary_over_len = 0;
    bool done = false;
    size_t buf_size = 0;
    while (!done) {
        auto buf = reader.GetBuffer(buf_size);
        if (buf_size == -1) {
            throw std::runtime_error("read error");
        }
        auto current_buf_data = buf->data;
        auto current_line_start = current_buf_data;

        done = adjust_buf(current_buf_data, buf_size);
        const char *end = current_buf_data + buf_size;

        for (; current_buf_data < end; current_buf_data += vector_size) {
            __mmask32 nl_mask = _mm256_cmpeq_epi8_mask(*((const __m256i *) (current_buf_data)), nl_vec);
            if (nl_mask == 0) {
                continue;
            }
            auto nl_pos = __builtin_ctz(nl_mask);
            if (cary_over_len == 0) {
                lp.ProcessLine(current_line_start);
            } else if (cary_over_len <= page_size) {
                lp.ProcessLine(buf->data - cary_over_len);
            } else {
                memcpy(cary_over+cary_over_len, buf->data, (current_buf_data - buf->data) + nl_pos);
                lp.ProcessLine(cary_over);
            }
            cary_over_len = 0;
            current_line_start = current_buf_data + nl_pos + 1;
        }
        cary_over_len = end - current_line_start;
        if (cary_over_len > 0) {
            if (done) {
                lp.ProcessLine(current_line_start);
            } else if (cary_over_len > page_size) {
                memcpy(cary_over, current_line_start, cary_over_len);
            }
        }
        reader.DoneWithBuffer(buf);
    }
}