#pragma once

#include <cstdlib>

enum class AllocMode {
    Alloc,
    MMap,
    MMapHuge,
};

enum class ReadMode {
    SingleThread,
    MultiThreaded,
};

enum class ParseMode {
    SAX,
    CharByChar,
    AVXShiftWithFallback,
    AVXRot,
};

constexpr AllocMode alloc_mode = AllocMode::MMapHuge;
constexpr ReadMode read_mode = ReadMode::SingleThread;
constexpr ParseMode parse_mode = ParseMode::AVXShiftWithFallback;

constexpr size_t vector_size = 32;
constexpr size_t page_size = 4096;
constexpr size_t huge_page_size = 2 * 1024 * 1024;


constexpr int num_mt_readers = 2;
constexpr size_t single_buffer_capacity = 64 * 1024 * 1024;
static_assert(single_buffer_capacity % huge_page_size == 0);

constexpr size_t num_buffers = 16;
static_assert(num_buffers > num_mt_readers + 1);

template<ReadMode rm> class Reader;
template<ParseMode pm> class Parser;
