#include <immintrin.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>

#include "const.h"
#include "st_reader.h"
#include "mt_reader.h"
#include "sax_parser.h"
#include "avx_shift_with_fallback_parser.h"
#include "char_by_char_parser.h"
#include "avx_rotr_parser.h"

int main(int argsLen, char** args) {
    int fd = open(args[1], O_RDONLY | O_DIRECT);
    if (fd == -1) {
        std::cerr << "can't open input" << std::endl;
        return 1;
    }
    posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);

    Reader<read_mode> reader(fd);
    Parser<parse_mode> parser;

    parser.ProcessReader(reader);
    close(fd);

    std::cout << parser.Result() << std::endl;
    return 0;
}
