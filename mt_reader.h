#pragma once

#include <atomic>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <unistd.h>
#include "alloc_data.h"

class MtBuffer {
public:
    std::atomic<size_t> offset;
    std::atomic<bool> ready_read;
    std::atomic<size_t> size;
    MtBuffer()
    : allocated((char*)alloc_data())
    , data(allocated+page_size)
    , offset(0)
    , ready_read(false)
    , size(single_buffer_capacity) {
    }
    ~MtBuffer() {
        free_data(allocated);
    }

private:
    char* const allocated;
public:
    char* const data;
};

class MtBuffers {
public:
    MtBuffers() : rr(0), wr(0) {}

    MtBuffer* BufForWrite() {
        // Determine the buf that we will be returning
        size_t seq = wr.fetch_add(1);
        MtBuffer* buf = &buffers[seq & (num_buffers - 1)];
        // Wait for the buf to become available, in 99.9% should be no block cause reader is much faster
        while ((seq - rr.load()) >= num_buffers) {
            std::unique_lock<std::mutex> lck(writeM);
            writeC.wait(lck);
        }

        // Let writer know what to pread
        buf->offset.store(seq * single_buffer_capacity);
        return buf;
    }

    MtBuffer* BufForRead() {
        // We have one reader so no need to fetch rr in the loop
        size_t seq = rr.load();
        MtBuffer* buf = &buffers[seq & (num_buffers - 1)];
        while (wr.load() == seq || !buf->ready_read.load()) {
            std::unique_lock<std::mutex> lck(readM);
            readC.wait(lck);
        }
        return buf;
    }

    void WriteDone() {
        readC.notify_one();
    }

    void ReadDone() {
        ++rr; // add_fetch
        writeC.notify_all();
    }

private:
    MtBuffer buffers[num_buffers];
    std::mutex readM;
    std::condition_variable readC;
    std::mutex writeM;
    std::condition_variable writeC;
    std::atomic<size_t> rr;
    std::atomic<size_t> wr;
};

template<>
class Reader<ReadMode::MultiThreaded> {
public:
    typedef MtBuffer BufferType;

    Reader(int fd) : fd(fd) {
        for (int i = 0; i < num_mt_readers; ++i) {
            std::thread reader([this]() {
                for (;;) {
                    auto buf = this->buffers.BufForWrite();
                    size_t sz = pread(this->fd, buf->data-page_size, single_buffer_capacity+2*page_size, buf->offset.load());
                    sz -= page_size;
                    buf->size.store(std::min(single_buffer_capacity, std::max<size_t>(sz, 0)));
                    buf->ready_read.store(true);
                    this->buffers.WriteDone();
                    if (sz <= 0) {
                        break;
                    }
                }
            });
            reader.detach();
        }
    }

    BufferType* GetBuffer(size_t& rd) {
        BufferType* result = buffers.BufForRead();
        rd = result->size.load();
        return result;
    }

    void DoneWithBuffer(BufferType* buf) {
        buf->ready_read.store(false);
        buffers.ReadDone();
    }

    int FD() { return fd; }

private:
    MtBuffers buffers;
    int fd;
};

